fetch('https://rss-api.fly.dev/page/fetchrss/ForeverbyIwonaNikelska')
    .then(response => response.json())
    .then(data => append_facebook_posts(data['items']))

function append_facebook_posts(data) {
    const filtered_data = data.filter(i => i.description || i.image)

    const loading_element = document.querySelector('.loading');
    loading_element.classList.add('display-none')

    const post_elements = document.querySelectorAll('.facebook-post')

    render_post(post_elements, filtered_data, 0);
    render_post(post_elements, filtered_data, 1);
}

function render_post(post_elements, data, index) {
    if (data.length > index) {

        post_elements[index].classList.remove('hidden')
        post_elements[index].href = data[index].url

        if (data[index].date) {
            const date = document.createElement('div')
            date.classList.add('facebook-date')
            date.innerText = data[index].date.substr(0, data[index].date.indexOf(' '))
            post_elements[index].append(date)
        }

        if (data[index].image) {
            const object = document.createElement('object')
            object.data = data[index].image
            object.type = 'image/jpeg'

            const fallback_img = document.createElement('img')
            fallback_img.src = 'media/facebook.png'
            fallback_img.classList.add('facebook-img')
            fallback_img.alt = 'Facebook post'
            object.append(fallback_img)
            post_elements[index].append(object)
        }

        if (data[index].description) {
            const text = document.createElement('p')
            text.innerText = data[index].description
            post_elements[index].append(text)
        }
    }
}