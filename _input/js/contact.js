const contactForm = document.querySelector('#contact-form');
const contactFormContent = document.querySelector('#contact-form textarea');

contactForm.addEventListener('submit', ev => {
    ev.preventDefault()

    fetch(contactForm.action, {
        method: contactForm.method,
        body: new FormData(contactForm)
    }).then(r => {
            if (r.status !== 200) {
                r.text().then(txt => {
                    alert("Błąd podczas wysyłania wiadaomości!\n\nSpróbuj ponownie lub skorzystaj z innej formy kontaktu.")
                    console.error(`Email sending error:\n${txt}`)
                })
            } else {
                alert("Wysłano wiadomość!")
                contactFormContent.value = ''
            }
        })
})