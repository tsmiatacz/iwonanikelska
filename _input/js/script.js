const footer = document.querySelector('footer')
const topButton = document.getElementById("top-button")

window.onscroll = function (ev) {
    if (window.scrollY + window.innerHeight >= document.body.scrollHeight - 5) {
        window.setTimeout(() => footer.classList.add('visible'), 100)
    }

    if (window.scrollY + window.innerHeight < document.body.scrollHeight - footer.scrollHeight) {
        footer.classList.remove('visible')
    }

    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        topButton.classList.add('visible')
    } else {
        topButton.classList.remove('visible')
    }
};

function goToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}