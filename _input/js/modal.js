const modalImg = document.getElementById('modal-img')
const modalDialog = document.getElementById('modal-dialog')

document.querySelectorAll('.certyfikaty>img')
    .forEach(img => img.addEventListener('click', _ => {
        modalImg.src = img.src
        modalDialog.classList.add('open')
        modalDialog.showModal()
    }))

document.getElementById('modal-close').addEventListener('click', _ => {
    modalDialog.classList.remove('open')
    modalDialog.close()
})

modalDialog.addEventListener('click', _ => {
    modalDialog.classList.remove('open')
    modalDialog.close()
})

document.querySelector('dialog section.modal').addEventListener('click', ev => ev.stopPropagation())