<?php
require 'vendor/autoload.php';

use \Mailjet\Resources;

if ($_POST["content"] && $_POST["name"] && $_POST["email"]) {

    $content = $_POST["content"];
    if ($_POST["phone"]) {
        $content .= "\n\nPodano numer telefonu: " . $_POST["phone"];
    }

    $mj = new \Mailjet\Client('c8982bb07993443375e90a4db2c87be4', '51141f136408de504b67453caede089f', true, ['version' => 'v3.1']);

    $body = [
        'Messages' => [
            [
                'From' => [
                    'Email' => "kontakt@iwonanikelska.pl",
                    'Name' => "Kontakt"
                ],
                'To' => [
                    [
                        'Email' => "iwonanikelska@gmail.com",
                        'Name' => "Iwona Nikelska"
                    ]
                ],
                'Headers' => [
                    'Reply-To' => $_POST["email"]
                ],
                'Subject' => "Wiadomość od: " . $_POST["name"],
                'TextPart' => $content
            ]
        ]
    ];

    try {
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        if ($response->success()) {
            echo $response->getBody();
            return;
        } else {
            http_response_code(500);
            echo $response->getReasonPhrase();
            return;
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo $e->getMessage();
        return;
    }
} else {
    http_response_code(400);
    echo "Missing values!";
    return;
}

