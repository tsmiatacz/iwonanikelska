module.exports = function(eleventyConfig) {

    eleventyConfig.addPassthroughCopy("_input/css/*.css");
    eleventyConfig.addPassthroughCopy("_input/js/**/*.*");
    eleventyConfig.addPassthroughCopy("_input/files/*.*");
    eleventyConfig.addPassthroughCopy("_input/media/*.*");
    eleventyConfig.addPassthroughCopy({"_external/service/": "/service/"});

    return {
        dir: {
            input: "_input",
            includes: "_includes",
            output: "_output",
            data: "_data"
        }
    }
};